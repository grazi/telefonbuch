package com.gfelline.telefonbuch.model

/**
 * Created by gfelline on 18.12.17.
 */
object IDGenerator {

    private var zaehler = 1


    fun generiereID(): Int {
        return zaehler++
    }

}