package com.gfelline.telefonbuch.model.db.api

import com.gfelline.telefonbuch.activities.CreateEintragView
import com.gfelline.telefonbuch.activities.EintraegeView
import com.gfelline.telefonbuch.activities.EintragView
import com.gfelline.telefonbuch.model.Person
import com.gfelline.telefonbuch.model.db.Datenbank
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RestApiDatenbank : Datenbank {

    private val eintragApiService by lazy {
        EintragApiService.create()
    }

    override fun personRegistrieren(person: Person, createEintragObserver: CreateEintragView) {

        val einEintrag = Eintrag(
                name = person.name,
                vorname = person.vorname,
                telefonnummer = person.telefonnummer,
                adresse = person.adresse,
                plz = person.plz,
                ort = person.ort);

        eintragApiService.createEintrag(einEintrag)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { eintrag ->
                            createEintragObserver.clearFieldsAndShowMsg()
                        },
                        { error -> throw Exception(error.message) }
                )
    }

    override fun findAllPersonen(eintraegeObserver: EintraegeView) {
        val personen = arrayListOf<Person>()
        val eintraege = eintragApiService.getAllEintraege()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            val personen = arrayListOf<Person>()
                            for (eintrag in result) {
                                personen.add(Person(eintrag.id!!.toInt(),
                                        eintrag.name,
                                        eintrag.vorname,
                                        eintrag.telefonnummer,
                                        eintrag.adresse,
                                        eintrag.plz,
                                        eintrag.ort))
                            }
                            eintraegeObserver.showEintraege(personen)
                        },
                        { error -> throw Exception(error.message) }
                )
    }

    override fun findPersonById(id: Int, eintragObserver: EintragView) {
        eintragApiService.findEintragById(id.toLong())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { eintrag ->

                            val person = Person(eintrag.id!!.toInt(),
                                    eintrag.name,
                                    eintrag.vorname,
                                    eintrag.telefonnummer,
                                    eintrag.adresse,
                                    eintrag.plz,
                                    eintrag.ort)

                            eintragObserver.showEintrag(person)

                        },
                        { error -> throw Exception(error.message) }
                )

    }
}