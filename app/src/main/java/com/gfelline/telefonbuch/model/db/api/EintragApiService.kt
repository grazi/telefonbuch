package com.gfelline.telefonbuch.model.db.api

import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Created by gfelline on 26.02.18.
 */
interface EintragApiService {

    @GET("eintraege")
    fun getAllEintraege() : Observable<List<Eintrag>>

    @GET("eintraege/{id}")
    fun findEintragById(@Path("id") id:Long) : Observable<Eintrag>

    @POST("eintraege")
    fun createEintrag(@Body eintrag: Eintrag) : Observable<Eintrag>

    companion object {
        fun create(): EintragApiService {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    //.baseUrl("http://192.168.1.109:8080/telefonbuch/")
                    .baseUrl("http://192.168.125.8:8080/telefonbuch/")
                    .build()

            return retrofit.create(EintragApiService::class.java)
        }
    }
}