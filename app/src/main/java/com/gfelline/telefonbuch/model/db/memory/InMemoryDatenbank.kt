package com.gfelline.telefonbuch.model.db.memory

import com.gfelline.telefonbuch.activities.CreateEintragView
import com.gfelline.telefonbuch.activities.EintraegeView
import com.gfelline.telefonbuch.activities.EintragView
import com.gfelline.telefonbuch.model.IDGenerator
import com.gfelline.telefonbuch.model.Person
import com.gfelline.telefonbuch.model.db.Datenbank

/**
 * Created by gfelline on 11.12.17.
 */
class InMemoryDatenbank : Datenbank {

    private val personen = arrayListOf<Person>()

    override fun personRegistrieren(person: Person, createEintragObserver: CreateEintragView) {
        person.setzeId(IDGenerator.generiereID())
        personen.add(person)
        createEintragObserver.clearFieldsAndShowMsg()
    }

    override fun findAllPersonen(eintraegeObserver: EintraegeView) {
        return eintraegeObserver.showEintraege(personen)
    }

    override fun findPersonById(id: Int, eintragObserver: EintragView) {
        for (einePerson in personen) {
            if (id == einePerson.id) {
                return eintragObserver.showEintrag(einePerson)
            }
        }
    }

}