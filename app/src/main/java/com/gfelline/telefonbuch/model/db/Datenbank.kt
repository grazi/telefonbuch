package com.gfelline.telefonbuch.model.db

import com.gfelline.telefonbuch.activities.CreateEintragView
import com.gfelline.telefonbuch.activities.EintraegeView
import com.gfelline.telefonbuch.activities.EintragView
import com.gfelline.telefonbuch.model.Person
import com.gfelline.telefonbuch.model.db.api.RestApiDatenbank

/**
 * Created by gfelline on 08.01.18.
 */
interface Datenbank {


    companion object {

        private val datenbank = RestApiDatenbank()

        fun getDb() : Datenbank {
            return datenbank
        }

    }

    fun personRegistrieren(person: Person, createEintragObserver: CreateEintragView)

    fun findAllPersonen(eintraegeObserver: EintraegeView)

    fun findPersonById(id: Int, eintragObserver: EintragView)

}