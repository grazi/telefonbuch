package com.gfelline.telefonbuch.model.db.sql

import com.gfelline.telefonbuch.activities.CreateEintragView
import com.gfelline.telefonbuch.activities.EintraegeView
import com.gfelline.telefonbuch.activities.EintragView
import com.gfelline.telefonbuch.model.Person
import com.gfelline.telefonbuch.model.db.Datenbank
import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.RowParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

/**
 * Created by gfelline on 29.01.18.
 */
class SqlDatenbank : Datenbank {

    private val dbHelfer = DatenbankHelfer.getInstance()

    override fun personRegistrieren(person: Person, createEintragObserver: CreateEintragView) {

        dbHelfer.use {
            insert(Person.TABLE_NAME,

                    Person.NAME to person.name,
                    Person.VORNAME to person.vorname,
                    Person.TELEFONNUMMER to person.telefonnummer,
                    Person.ADRESSE to person.adresse,
                    Person.ORT to person.ort,
                    Person.PLZ to person.plz)
        }
        createEintragObserver.clearFieldsAndShowMsg()
    }

    override fun findAllPersonen(eintraegeObserver: EintraegeView) {
        val allePersonen = arrayListOf<Person>()

        dbHelfer.use {
            select(Person.TABLE_NAME)
                    .parseList(object : MapRowParser<List<Person>> {

                        override fun parseRow(columns: Map<String, Any?>): List<Person> {

                            val person = parsePerson(columns)
                            allePersonen.add(person)

                            return arrayListOf()

                        }

                    })
        }
        return eintraegeObserver.showEintraege(allePersonen)
    }

    private fun parsePerson(columns: Map<String, Any?>) :Person {
        val id = columns.getValue(Person.ID)
        val name = columns.getValue(Person.NAME)
        val vorname = columns.getValue(Person.VORNAME)
        val adresse = columns.getValue(Person.ADRESSE)
        val telefonnummer = columns.getValue(Person.TELEFONNUMMER)
        val ort = columns.getValue(Person.ORT)
        val plz = columns.getValue(Person.PLZ)

        return Person(id= id.toString().toInt(),
                name = name.toString(),
                vorname = vorname.toString(),
                adresse = adresse.toString(),
                telefonnummer = telefonnummer.toString(),
                ort = ort.toString(),
                plz = plz.toString())

    }

    private fun parsePerson(columns: Array<Any?>) : Person {
        val id = columns.get(0)
        val name = columns.get(1)
        val vorname = columns.get(2)
        val adresse = columns.get(3)
        val telefonnummer = columns.get(4)
        val ort = columns.get(5)
        val plz = columns.get(6)

        return Person(id= id.toString().toInt(),
                name = name.toString(),
                vorname = vorname.toString(),
                adresse = adresse.toString(),
                telefonnummer = telefonnummer.toString(),
                ort = ort.toString(),
                plz = plz.toString())
    }

    override fun findPersonById(id: Int, eintragObserver: EintragView) {


        var person:Person? = null

        dbHelfer.use {
            select(Person.TABLE_NAME)
                    .whereSimple("${Person.ID} = ?", "$id")
                    .parseOpt(object : RowParser<Person> {
                        override fun parseRow(columns: Array<Any?>): Person {
                            person  = parsePerson(columns)
                            return person as Person
                        }

                    })
        }

        eintragObserver.showEintrag(Person(vorname = "Graziano", name = "Felline", adresse = "bingho", telefonnummer = "bb", ort = "luzern", plz = ""))
    }
}