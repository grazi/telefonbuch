package com.gfelline.telefonbuch.model.db.api

/**
 * Created by gfelline on 26.02.18.
 */
data class Eintrag(val id:Long? = null,
                   val name:String,
                   val vorname:String,
                   val telefonnummer:String,
                   val adresse:String,
                   val plz:String,
                   val ort:String)