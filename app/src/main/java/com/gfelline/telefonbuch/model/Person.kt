package com.gfelline.telefonbuch.model

/**
 * Created by gfelline on 11.12.17.
 */
class Person(
        var id:Int? = null,
        val name:String,
        val vorname:String,
        val telefonnummer:String,
        val adresse:String,
        val plz:String,
        val ort:String) {


    companion object {
        val TABLE_NAME: String = "person"
        val ID : String = "_id"
        val NAME:String = "name"
        val VORNAME:String = "vorname"
        val TELEFONNUMMER:String = "telefonnummer"
        val ADRESSE:String = "adresse"
        val PLZ:String = "plz"
        val ORT:String = "ort"
    }

    fun setzeId(eineID:Int) {
        id = eineID
    }

}



