package com.gfelline.telefonbuch.model.db.sql

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.gfelline.telefonbuch.app.TelefonbuchApp
import com.gfelline.telefonbuch.model.Person
import org.jetbrains.anko.db.*

/**
 * Created by gfelline on 29.01.18.
 */
class DatenbankHelfer(ctx: Context = TelefonbuchApp.instance) : ManagedSQLiteOpenHelper(ctx, "telefonbuch") {

    companion object {

        fun getInstance() : DatenbankHelfer {
            return DatenbankHelfer()
        }

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    override fun onCreate(db: SQLiteDatabase?) {

        if (db != null) {
            db.createTable(
                    Person.TABLE_NAME, true,
                    Person.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                    Person.NAME to TEXT,
                    Person.VORNAME to TEXT,
                    Person.TELEFONNUMMER to TEXT,
                    Person.ADRESSE to TEXT,
                    Person.PLZ to TEXT,
                    Person.ORT to TEXT
            )
        }


    }
}