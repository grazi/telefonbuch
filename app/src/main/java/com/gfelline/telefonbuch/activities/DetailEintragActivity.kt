package com.gfelline.telefonbuch.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.gfelline.telefonbuch.R
import com.gfelline.telefonbuch.model.Person
import com.gfelline.telefonbuch.model.db.Datenbank
import kotlinx.android.synthetic.main.activity_detail_eintrag.*


class DetailEintragActivity : AppCompatActivity(), EintragView {
    override fun showEintrag(person: Person) {
        if (person != null) {
            setzeDetails(person)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_eintrag)


        val personId = intent.getStringExtra("person-id")

        Datenbank.getDb().findPersonById(personId.toInt(), this)

    }


    fun setzeDetails(person : Person) {
        persIdTxt.setText("${person.id}")
        telEintragNameTxt.setText(person.name)
        telEintragAdresseTxt.setText(person.adresse)
        telEintragOrtTxt.setText(person.ort)
        telEintragVornameTxt.setText(person.vorname)
        telEintragPlzTxt.setText(person.plz)
        telEintragTelefonnummerTxt.setText(person.telefonnummer)
    }

}
