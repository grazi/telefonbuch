package com.gfelline.telefonbuch.activities

import com.gfelline.telefonbuch.model.Person

/**
 * Created by gfelline on 26.02.18.
 */
interface EintraegeView {

    fun showEintraege(personen : ArrayList<Person>)

}