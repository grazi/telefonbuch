package com.gfelline.telefonbuch.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.gfelline.telefonbuch.R
import com.gfelline.telefonbuch.model.Person
import com.gfelline.telefonbuch.model.db.Datenbank
import kotlinx.android.synthetic.main.activity_eintraege.*
import kotlinx.android.synthetic.main.view_eintrag.view.*
import java.util.*

class EintraegeActivity : AppCompatActivity(), EintraegeView {

    override fun showEintraege(personen : ArrayList<Person>) {
        listView.adapter = PersonenAdapter(this, personen)

        listView.setOnItemClickListener { parent, view, position, id ->

            val goToDetails = Intent(this, DetailEintragActivity::class.java)
            val personId = view.eintragIdTxt.text.toString()
            goToDetails.putExtra("person-id", personId)
            startActivity(goToDetails)

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eintraege)
        Datenbank.getDb().findAllPersonen(this)
    }
}
