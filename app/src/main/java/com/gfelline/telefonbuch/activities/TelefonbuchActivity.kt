package com.gfelline.telefonbuch.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.gfelline.telefonbuch.R
import kotlinx.android.synthetic.main.activity_telefonbuch.*

class TelefonbuchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_telefonbuch)

    }


    fun neuerEintragBtnClicked(view:View) {
        val goToNeuerEintrag = Intent(this, NeuerEintragActivity::class.java)
        startActivity(goToNeuerEintrag)
    }

    fun telefonBuchBtnClicked(view: View) {
        val goToTelefonBuch = Intent(this, EintraegeActivity::class.java)
        startActivity(goToTelefonBuch)
    }

}
