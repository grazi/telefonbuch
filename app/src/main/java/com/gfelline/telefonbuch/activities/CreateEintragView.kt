package com.gfelline.telefonbuch.activities

/**
 * Created by gfelline on 04.03.18.
 */
interface CreateEintragView {

    fun clearFieldsAndShowMsg()
}