package com.gfelline.telefonbuch.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.gfelline.telefonbuch.R
import com.gfelline.telefonbuch.model.Person
import com.gfelline.telefonbuch.model.db.Datenbank
import kotlinx.android.synthetic.main.activity_neuer_eintrag.*

class NeuerEintragActivity : AppCompatActivity(), CreateEintragView {

    override fun clearFieldsAndShowMsg() {
        clearTxtFields()
        Toast.makeText(this, "Person hinzugefügt!", Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_neuer_eintrag)
    }

    fun registierenBtnClicked(view:View) {

        Datenbank.getDb().personRegistrieren(Person(
                name = nameTxt.text.toString(),
                vorname = vornameTxt.text.toString(),
                telefonnummer = telefonnummerTxt.text.toString(),
                adresse = adresseTxt.text.toString(),
                plz = plzTxt.text.toString(),
                ort = ortTxt.text.toString()
        ), this)


    }

    private fun clearTxtFields() {
        nameTxt.text.clear()
        vornameTxt.text.clear()
        telefonnummerTxt.text.clear()
        adresseTxt.text.clear()
        plzTxt.text.clear()
        ortTxt.text.clear()
    }
}
