package com.gfelline.telefonbuch.activities


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.gfelline.telefonbuch.R
import com.gfelline.telefonbuch.model.Person


/**
 * Created by gfelline on 11.12.17.
 */
class PersonenAdapter(context: Context, personen:ArrayList<Person>): ArrayAdapter<Person>(context, 0, personen) {


    override fun getView(position: Int, wiederZuVerwendendeView: View?, parent: ViewGroup?): View {
        val neueAnsichtView: View

        if (wiederZuVerwendendeView == null) {
            neueAnsichtView = LayoutInflater.from(context).inflate(R.layout.view_eintrag, parent, false)
        } else {
            neueAnsichtView = wiederZuVerwendendeView
        }


        val person = getItem(position)
        val eintragId = neueAnsichtView.findViewById<TextView>(R.id.eintragIdTxt)
        eintragId.setText("${person.id}")

        val eintragName = neueAnsichtView.findViewById<TextView>(R.id.eintragNameTxt)
        eintragName.setText(person.name)

        val eintragTelefonummer = neueAnsichtView.findViewById<TextView>(R.id.eintragTelefonummerTxt)
        eintragTelefonummer.setText(person.telefonnummer)

        return neueAnsichtView
    }


}