package com.gfelline.telefonbuch.activities

import com.gfelline.telefonbuch.model.Person

/**
 * Created by gfelline on 04.03.18.
 */
interface EintragView {

    fun showEintrag(person : Person)

}