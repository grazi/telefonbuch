package com.gfelline.telefonbuch.app

import android.app.Application

/**
 * Created by gfelline on 29.01.18.
 */
class TelefonbuchApp : Application() {

    companion object {
        lateinit var instance: TelefonbuchApp
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}